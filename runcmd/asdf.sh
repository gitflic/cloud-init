#!/usr/bin/env sh
# shellcheck disable=all

appname=$(basename "$0")
cwd=$(pwd)

if command -v tput >/dev/null 2>&1; then
  reset=$(tput sgr0)
  black=$(tput setaf 0)
  red=$(tput setaf 1)
  green=$(tput setaf 2)
  yellow=$(tput setaf 3)
  blue=$(tput setaf 4)
  magenta=$(tput setaf 5)
  cyan=$(tput setaf 6)
  white=$(tput setaf 7)
fi

if [ -n "$(command -v apt-get)" ]; then
  export DEBIAN_FRONTEND=noninteractive
  export ASDF_DIR=${HOME}/.asdf
  export PATH="${PATH}:${ASDF_DIR}/bin:${ASDF_DIR}/shims"
  export PATH="$HOME/.pyenv/bin:$PATH"
  export PIP_ROOT_USER_ACTION=ignore
  export ASDF_GOLANG_MOD_VERSION_ENABLED=true
  export ASDF_PYAPP_INCLUDE_DEPS=1
  export ASDF_GOLANG_OVERWRITE_ARCH=amd64
  export ASDF_DIRENV_IGNORE_MISSING_PLUGINS=1
  export ASDF_POETRY_INSTALL_URL=https://install.python-poetry.org
  export PATH="$HOME/.asdf/installs/poetry/1.8.3/bin:$PATH"
  export ASDF_GOLANG_SKIP_CHECKSUM=1
  export ASDF_CONCURRENCY=1
fi

install_asdf_versions() {
  setup_asdf_version python 3.11.2
}

setup_asdf_version() {
  local plugin=${1}
  local version=${2}
  local plugin_url=${3:-""}

  asdf plugin add "${plugin}" "${plugin_url}"
  asdf install "${plugin}" "${version}"
  asdf global "${plugin}" "${version}"
}

_install_asdf(){
if which asdf >/dev/null; then
  echo " ${white}[${yellow}✖️${white}] ${cyan}Asdf ${white}уже установлен${reset}"
  return 0
else
  echo " ${white}[${cyan}🛠️${white}] ${cyan}Asdf ${white}устанавливается...${reset}"
  [ -d $HOME/.asdf ] && rm -rf $HOME/.asdf

  if [ ! -d "$HOME/.asdf" ]; then
  asdf_version=$(curl -s https://api.github.com/repos/asdf-vm/asdf/releases/latest | jq -r '.tag_name')
  git clone "https://github.com/asdf-vm/asdf.git" "$HOME/.asdf" --branch $asdf_version 2> /dev/null
  cd $HOME/.asdf && git checkout "$(git describe --abbrev=0 --tags)" 2> /dev/null
  fi

  [ -f $HOME/.asdfrc ] && rm -rf $HOME/.asdfrc
  [ -f $HOME/.tool-versions ] && rm -rf $HOME/.tool-versions
  [ -f $HOME/.asdf/help.txt ] && rm -rf $HOME/.asdf/help.txt
  [ -f $HOME/.asdf/lib/commands/command-help.bash ] && \
  rm -rf $HOME/.asdf/lib/commands/command-help.bash

  [ -d $HOME/.asdf/.github ] && rm -rf $HOME/.asdf/.github && rm -rf $HOME/.asdf/docs
  [ -f $HOME/.asdf/asdf.elv ] && rm -rf $HOME/.asdf/asdf.elv && rm -rf $HOME/.asdf/asdf.fish && \
  rm -rf $HOME/.asdf/asdf.nu && rm -rf $HOME/.asdf/asdf.ps1
  [ -f $HOME/.asdf/.editorconfig ] && rm -rf $HOME/.asdf/.editorconfig && rm -rf $HOME/.asdf/.gitattributes && \
  rm -rf $HOME/.asdf/.git-blame-ignore-revs && rm -rf $HOME/.asdf/.gitignore
  [ -f $HOME/.asdf/README.md ] && rm -rf $HOME/.asdf/README.md && rm -rf $HOME/.asdf/SECURITY.md && \
  rm -rf $HOME/.asdf/CONTRIBUTING.md && rm -rf $HOME/.asdf/CHANGELOG.md && rm -rf $HOME/.asdf/ballad-of-asdf.md

  sudo su -c "bash -x <(curl -sSL -o $HOME/.asdfrc https://gitlab.com/gitflic/asdf/-/raw/main/.asdfrc)" root
  sudo su -c "bash -x <(curl -sSL -o $HOME/.tool-versions https://gitlab.com/gitflic/asdf/-/raw/main/.tool-versions)" root
  sudo su -c "bash -x <(curl -sSL -o $HOME/.asdf/help.txt https://gitlab.com/gitflic/asdf/-/raw/main/help.txt)" root
  sudo su -c "bash -x <(curl -sSL -o $HOME/.asdf/lib/commands/command-help.bash https://gitlab.com/gitflic/asdf/-/raw/main/command-help.bash)" root

  [ -f $HOME/.wget-hsts ] && rm -rf $HOME/.wget-hsts
  [ -f $HOME/wget-log ] && rm -rf $HOME/wget-log
  [ -f $HOME/resize.log ] && rm -rf $HOME/resize.log

  echo " ${white}[${green}✔️${white}] ${cyan}Asdf ${white}установлен${reset}"
fi
}
_install_asdf
