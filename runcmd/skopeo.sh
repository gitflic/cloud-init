#!/usr/bin/env sh
# shellcheck disable=all

appname=$(basename "$0")
cwd=$(pwd)

if command -v tput >/dev/null 2>&1; then
  reset=$(tput sgr0)
  black=$(tput setaf 0)
  red=$(tput setaf 1)
  green=$(tput setaf 2)
  yellow=$(tput setaf 3)
  blue=$(tput setaf 4)
  magenta=$(tput setaf 5)
  cyan=$(tput setaf 6)
  white=$(tput setaf 7)
fi

if [ -n "$(command -v apt-get)" ]; then
  export DEBIAN_FRONTEND=noninteractive
  export ASDF_DIR=${HOME}/.asdf
  export PATH="${PATH}:${ASDF_DIR}/bin:${ASDF_DIR}/shims"
  export PATH="$HOME/.pyenv/bin:$PATH"
  export PIP_ROOT_USER_ACTION=ignore
  export ASDF_GOLANG_MOD_VERSION_ENABLED=true
  export ASDF_PYAPP_INCLUDE_DEPS=1
  export ASDF_GOLANG_OVERWRITE_ARCH=amd64
  export ASDF_DIRENV_IGNORE_MISSING_PLUGINS=1
  export ASDF_POETRY_INSTALL_URL=https://install.python-poetry.org
  export PATH="$HOME/.asdf/installs/poetry/1.8.3/bin:$PATH"
  export ASDF_GOLANG_SKIP_CHECKSUM=1
  export ASDF_CONCURRENCY=1
  export always_keep_download=yes
fi

_install_go() {
  if which go >/dev/null 2>&1; then
    echo " ${white}[${yellow}+${white}] ${cyan}Go ${white}уже установлен${reset}"
    return 0
  else
    echo " ${white}[${cyan}🛠️${white}] ${cyan}Go ${white}устанавливается...${reset}"
    asdf plugin add go https://github.com/asdf-community/asdf-golang.git >/dev/null 2>&1
    asdf install go >/dev/null 2>&1 $(asdf latest go)
    asdf global go >/dev/null 2>&1 $(asdf latest go)
    asdf reshim go >/dev/null 2>&1
    echo " ${white}[${green}✔️${white}] ${cyan}Go ${white}установлен${reset}"
  fi
}

_install_skopeo() {
if which skopeo >/dev/null; then
  echo " ${white}[${yellow}+${white}] ${cyan}skopeo ${white}уже установлен${reset}"
  return 0
else
  echo " ${white}[${cyan}🛠️${white}] ${cyan}skopeo ${white}устанавливается...${reset}"
  asdf plugin add skopeo https://github.com/kheino/asdf-skopeo >/dev/null 2>&1
  asdf install skopeo latest >/dev/null 2>&1
  asdf global skopeo latest >/dev/null 2>&1
  echo " ${white}[${green}✔️${white}] ${cyan}skopeo ${white}установлен${reset}"
fi
}
_install_go && _install_skopeo
