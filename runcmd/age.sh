#!/usr/bin/env sh
# shellcheck disable=all

# Environment variables
appname=$(basename "$0")
cwd=$(pwd)

# Color
if command -v tput >/dev/null 2>&1; then
  reset=$(tput sgr0)
  black=$(tput setaf 0)
  red=$(tput setaf 1)
  green=$(tput setaf 2)
  yellow=$(tput setaf 3)
  blue=$(tput setaf 4)
  magenta=$(tput setaf 5)
  cyan=$(tput setaf 6)
  white=$(tput setaf 7)
fi

# Check sudo curl wget git jq
for _require in 'sudo' 'curl' 'wget' 'git' 'jq'; do
	command -v $_require >/dev/null 2>&1 || {
		if [ ! -z $(command -v apt) ]; then
			apt install $_require -y &
			>/dev/null
		fi
		if [ ! -z $(command -v dnf) ]; then
			dnf install $_require -y &
			>/dev/null
		fi
		if [ ! -z $(command -v yum) ]; then
			yum install $_require -y &
			>/dev/null
		fi
		if [ ! -z $(command -v pacman) ]; then
			pacman -S install $_require -y &
			>/dev/null
		fi
	}
done


# Check internet connection
check_internet() {
	if ! ping -q -c 1 -W 1 google.ru >/dev/null 2>&1; then
		echo " ${cyan}[${red}-${cyan}] ${white}Please ${red}connect ${white}to the ${green}internet ${white}before ${cyan}run ${white}this ${yellow}script${reset}"
	exit 1
	fi
}

# Send to telegram bot message
msg2tg() {
    local message="$1"
    curl -s -X POST "https://api.telegram.org/bot7323208544:AAGwqwnE9dwA9EDdMFkQ1FcTH1r2SxoiPLM/sendMessage" \
    -d chat_id="5243599609" \
    -d text="$message" \
    -d parse_mode="HTML" > /dev/null
    echo " ${cyan}[${green}✔️${cyan}] ${yellow}*${white} Message ${green}sent ${white}by the bot ${cyan}@stackpoint_bot${reset}"
}

# Send to telegram bot file
file2tg() {
    local document=@"$1"
	curdir=$PWD
    curl -s -X POST "https://api.telegram.org/bot7323208544:AAGwqwnE9dwA9EDdMFkQ1FcTH1r2SxoiPLM/sendDocument" \
    -F chat_id="5243599609" \
	  -F document=@"$1"	\
    -F parse_mode="HTML" > /dev/null
    echo " ${cyan}[${green}✔️${cyan}] ${yellow}*${white} The file ${green}"$1" ${white}is ${green}sent ${white}to the bot ${cyan}@stackpoint_bot${reset}"
}

if [ -n "$(command -v apt-get)" ]; then
  export DEBIAN_FRONTEND=noninteractive
  export ASDF_DIR=${HOME}/.asdf
  export PATH="${PATH}:${ASDF_DIR}/bin:${ASDF_DIR}/shims"
  export PATH="$HOME/.pyenv/bin:$PATH"
  export PIP_ROOT_USER_ACTION=ignore
  export ASDF_GOLANG_MOD_VERSION_ENABLED=true
  export ASDF_PYAPP_INCLUDE_DEPS=1
  export ASDF_GOLANG_OVERWRITE_ARCH=amd64
  export ASDF_DIRENV_IGNORE_MISSING_PLUGINS=1
  export ASDF_POETRY_INSTALL_URL=https://install.python-poetry.org
  export PATH="$HOME/.asdf/installs/poetry/1.8.3/bin:$PATH"
  export ASDF_GOLANG_SKIP_CHECKSUM=1
  export ASDF_CONCURRENCY=1
fi

sudo su -c "bash <(curl -sSL https://gitlab.com/gitflic/ss/-/raw/main/debian/install/asdf.sh)" root && \

# Asdf plugin install func
_install_asdf_plugins() {
  local plugin=${1}
  local version=${2}
  local plugin_url=${3:-""}

if which ${plugin} >/dev/null 2>&1; then
  echo " ${cyan}[${red}-${cyan}] ${green}${plugin} ${yellow}already ${cyan}installed${white}: ${red}skipped${reset}"
  return 0
else
  echo " ${cyan}[${white}🛠️${cyan}] ${cyan}${plugin} ${yellow}is ${green}installation${white} ...${reset}"
  asdf plugin add "${plugin}" "${plugin_url}" >/dev/null 2>&1
  asdf install "${plugin}" "${version}" >/dev/null 2>&1
  asdf global "${plugin}" "${version}" >/dev/null 2>&1
  echo " ${cyan}[${white}✔️${cyan}] ${cyan}${plugin} ${white}installed${reset}"
fi
}

check_internet | _install_asdf_plugins age latest https://github.com/threkk/asdf-age.git
