#!/usr/bin/env sh
# shellcheck disable=all

# Environment variables
appname=$(basename "$0")
cwd=$(pwd)

# Load Core
#if [ -x "core.sh" ]; then
#  . "core.sh" || exit 1
#else
#  download_core || exit 1
#fi

# Check sudo curl wget git jq
for _require in 'sudo' 'curl' 'wget' 'git' 'jq'; do
	command -v $_require >/dev/null 2>&1 || {
		if [ ! -z $(command -v apt) ]; then
			apt install $_require -y &
			>/dev/null
		fi
		if [ ! -z $(command -v dnf) ]; then
			dnf install $_require -y &
			>/dev/null
		fi
		if [ ! -z $(command -v yum) ]; then
			yum install $_require -y &
			>/dev/null
		fi
		if [ ! -z $(command -v pacman) ]; then
			pacman -S install $_require -y &
			>/dev/null
		fi
	}
done

# Colors
if command -v tput >/dev/null 2>&1; then
  reset=$(tput sgr0)
  black=$(tput setaf 0)
  red=$(tput setaf 1)
  green=$(tput setaf 2)
  yellow=$(tput setaf 3)
  blue=$(tput setaf 4)
  magenta=$(tput setaf 5)
  cyan=$(tput setaf 6)
  white=$(tput setaf 7)
fi

if [ -n "$(command -v apt-get)" ]; then
  export DEBIAN_FRONTEND=noninteractive
  export ASDF_DIR=${HOME}/.asdf
  export PATH="${PATH}:${ASDF_DIR}/bin:${ASDF_DIR}/shims"
  export PATH="$HOME/.pyenv/bin:$PATH"
  export PIP_ROOT_USER_ACTION=ignore
  export ASDF_GOLANG_MOD_VERSION_ENABLED=true
  export ASDF_PYAPP_INCLUDE_DEPS=1
  export ASDF_GOLANG_OVERWRITE_ARCH=amd64
  export ASDF_DIRENV_IGNORE_MISSING_PLUGINS=1
  export ASDF_POETRY_INSTALL_URL=https://install.python-poetry.org
  export PATH="$HOME/.asdf/installs/poetry/1.8.3/bin:$PATH"
  export ASDF_GOLANG_SKIP_CHECKSUM=1
  export ASDF_CONCURRENCY=1
fi

# Func Downloads
dl() {
  if command -v curl > /dev/null 2>&1; then
    curl -sSL "$1"  >/dev/null 2>&1
  elif command -v wget > /dev/null 2>&1; then
    wget -qO- "$1" >/dev/null 2>&1
  else
	echo " ${cyan}[${red}-${cyan}] ${white}No ${red}curl ${white}or ${red}wget ${white}found${white} ${reset}"
  fi
}

# Check internet connection
check_internet() {
  local url=${1}
	if ! ping -q -c 1 -W 1 $url >/dev/null 2>&1; then
		echo " ${cyan}[${red}-${cyan}] ${white}Please ${green}check ${white}server ${yellow}internet ${white}connection${reset}"
	  exit 1
	fi
}

# Check website connection
check_website() {
  local url=${1}
  if curl -s --head  --request GET $url | grep "200 OK" > /dev/null; then 
    echo " ${cyan}[${white}✔️${cyan}] $url ${green}It works${reset}"
    exit 1
  else
    echo " ${cyan}[${red}-${cyan}] $url ${red}down${reset}"
    exit 1
  fi
}

# Check Distro
distro=$(cat /etc/os-release | tr [:upper:] [:lower:] | grep -Poi '(debian|ubuntu|red hat|centos|arch|alpine)' | uniq)

# Check ipaddr
ipaddr() {
	local IP=$(ip addr | egrep -o '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' | egrep -v "^192\.168|^172\.1[6-9]\.|^172\.2[0-9]\.|^172\.3[0-2]\.|^10\.|^127\.|^255\.|^0\." | head -n 1)
	[ -z ${IP} ] && IP=$(wget -qO- -t1 -T2 ipv4.icanhazip.com)
	[ -z ${IP} ] && IP=$(wget -qO- -t1 -T2 ipinfo.io/ip)
	[ ! -z ${IP} ] && echo ${IP} || echo
}

# Check hostname
check_hostname() {
  hostnamectl | egrep -i "Static hostname" | awk '{print $NF}'
}

# Send to telegram bot message
msg2tg() {
    local message="$1"
    curl -s -X POST "https://api.telegram.org/bot7323208544:AAGwqwnE9dwA9EDdMFkQ1FcTH1r2SxoiPLM/sendMessage" \
    -d chat_id="5243599609" \
    -d text="$message" \
    -d parse_mode="HTML" > /dev/null
    echo " ${cyan}[${white}✔️${cyan}] ${white}Messages send${reset}"
}

# Send to telegram bot file
file2tg() {
    local document=@"$1"
	curdir=$PWD
    curl -s -X POST "https://api.telegram.org/bot7323208544:AAGwqwnE9dwA9EDdMFkQ1FcTH1r2SxoiPLM/sendDocument" \
    -F chat_id="5243599609" \
	  -F document=@"$1"	\
    -F parse_mode="HTML" > /dev/null
    echo " ${cyan}[${white}✔️${cyan}] ${white}The file ${green}"$1" ${white}is sent${reset}"
}

_install_pget() {
if which pget >/dev/null; then
  echo " ${cyan}[${red}-${cyan}] ${green}pget ${yellow}already ${cyan}installed${white}: ${red}skipped${reset}"
  return 0
else
  echo " ${cyan}[${white}🛠️${cyan}] ${cyan}pget ${yellow}is ${green}installation${white} ...${reset}"
  [ -f /usr/local/bin/pget ] && rm -rf /usr/local/bin/pget
  curl -sSL -o /usr/local/bin/pget https://gitlab.com/gitflic/dotfiles/-/raw/main/usr/local/bin/pget && \
  chown -R root:root /usr/local/bin/pget && chmod 755 /usr/local/bin/pget
  echo " ${cyan}[${white}✔️${cyan}] ${cyan}pget ${white}installed${reset}"
fi
}
check_internet | _install_pget
